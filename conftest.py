# content of conftest.py
import pytest
import pycblib


@pytest.fixture
def keys():
    ''' Generated keys '''
    curves = [pycblib.P256, pycblib.P384, pycblib.P521]
    keys = {}
    for curve in curves:
        keys[curve] = pycblib.generate_keys(curve)

    return keys
