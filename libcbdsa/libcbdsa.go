package main

/*
#include <stdbool.h>
typedef struct {
	char* PrivateKey;
	char* PublicKey;
} CKey;

*/
import "C"
import (
	"bitbucket.org/rmanolis/cryptobureau-cli/cbdsa"
	"crypto/rand"
	"unsafe"
)

type Key C.CKey

//export cb_generate_keys
func cb_generate_keys(curve_name *C.char, result *Key, cerr **C.char) C.int {
	key, err := cbdsa.GenerateKeys(C.GoString(curve_name), rand.Reader)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	result.PrivateKey = C.CString(string(key.PrivateKey))
	result.PublicKey = C.CString(string(key.PublicKey))
	return 0
}

//export cb_sign
func cb_sign(private_key *C.char, file unsafe.Pointer, length C.int, signature **C.char, cerr **C.char) C.int {
	f := C.GoBytes(file, length)
	pk := C.GoString(private_key)
	s, err := cbdsa.Sign(f, []byte(pk), rand.Reader)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*signature = C.CString(string(s))
	return 0
}

func Btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

//export cb_verify
func cb_verify(public_key *C.char, file unsafe.Pointer, length C.int, signature *C.char, verified *C.int, cerr **C.char) C.int {
	f := C.GoBytes(file, length)
	pub := C.GoString(public_key)
	sig := C.GoString(signature)
	b, err := cbdsa.Verify([]byte(pub), f, []byte(sig))
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*verified = C.int(Btoi(b))
	return 0
}

//export cb_encrypt
func cb_encrypt(public_key *C.char, file unsafe.Pointer, length C.int, encrypted **C.char, cerr **C.char) C.int {
	f := C.GoBytes(file, length)
	pub := C.GoString(public_key)
	b, err := cbdsa.Encrypt([]byte(pub), f)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*encrypted = C.CString(string(b))
	return 0
}

//export cb_decrypt
func cb_decrypt(private_key *C.char, file unsafe.Pointer, length C.int, decrypted **C.char, cerr **C.char) C.int {
	f := C.GoBytes(file, length)
	pub := C.GoString(private_key)

	b, err := cbdsa.Decrypt([]byte(pub), f)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*decrypted = C.CString(string(b))
	return 0
}

//export cb_encrypt_cbc
func cb_encrypt_cbc(secret *C.char, text *C.char, encrypted **C.char, cerr **C.char) C.int {
	s := C.GoString(secret)
	t := C.GoString(text)
	b, err := cbdsa.EncryptCBC([]byte(s), t)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*encrypted = C.CString(string(b))
	return 0
}

//export cb_decrypt_cbc
func cb_decrypt_cbc(secret *C.char, text *C.char, decrypted **C.char, cerr **C.char) C.int {
	s := C.GoString(secret)
	t := C.GoString(text)
	b, err := cbdsa.DecryptCBC([]byte(s), t)
	if err != nil {
		*cerr = C.CString(err.Error())
		return 1
	}
	*decrypted = C.CString(string(b))
	return 0
}

//export cb_compute_hmac
func cb_compute_hmac(secret *C.char, text *C.char, hmac **C.char) {
	s := C.GoString(secret)
	t := C.GoString(text)
	b := cbdsa.ComputeHmac256([]byte(s), []byte(t))
	*hmac = C.CString(string(b))
}

//export cb_check_hmac
func cb_check_hmac(secret *C.char, text *C.char, text_mac *C.char) C.int {
	s := C.GoString(secret)
	t := C.GoString(text)
	tm := C.GoString(text_mac)
	b := cbdsa.CheckHmac256([]byte(s), []byte(t), tm)

	return C.int(Btoi(b))
}

func main() {
}
