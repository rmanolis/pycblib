from distutils.core import setup
from distutils.sysconfig import get_python_inc
import os

if not os.path.isfile("libcbdsa/libcbdsa.so"):
    os.system("go get bitbucket.org/rmanolis/cblib")
    os.system(
        "go build -buildmode=c-shared -o  libcbdsa/libcbdsa.so libcbdsa/libcbdsa.go")

df = os.path.join(os.path.dirname(get_python_inc()), "lib")

setup(name='pycblib',
      version='0.1',
      license='Apache 2.0',
      description='Cryptobureau library',
      author='Manos Ragiadakos',
      author_email='manos.ragiadakos@gmail.com',
      url='https://bitbucket.org/rmanolis/pycblib',
      packages=['pycblib'],
      # package_data={'pycblib': ['libcbdsa/*.so']},
      data_files=[(df, ['libcbdsa/libcbdsa.so'])]
      )
