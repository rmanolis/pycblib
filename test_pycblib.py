import pycblib
import pytest

def test_signature(keys):
    text = bytearray(b'lalalalalala')
    for key in keys.values():
        signature = pycblib.sign(key.privateKey, text)
        b = pycblib.verify(key.publicKey, text, signature)
        assert b is True
        b = pycblib.verify(key.publicKey, bytearray(b'lalala'), signature)
        assert b is False


def test_file_signature(keys):
    with open(".gitignore", "rb") as file:
        f = file.read()
        b = bytearray(f)

    for key in keys.values():
        signature = pycblib.sign(key.privateKey, b)
        is_true = pycblib.verify(key.publicKey, b, signature)
        assert is_true


def test_encryption(keys):
    text = bytearray(b'lalal')
    for key in keys.values():
        enc = pycblib.encrypt(key.publicKey, text)
        dec = pycblib.decrypt(key.privateKey, enc)
        assert text == dec


def test_cbc_encryption():
    secret = "lala"
    text = "aaaaaa"
    enc = pycblib.encrypt_cbc(secret, text)
    dec = pycblib.decrypt_cbc(secret, enc)
    assert text == dec


def test_hmac():
    secret = "lala"
    text = "aaaaa"
    h = pycblib.compute_hmac(secret, text)
    is_true = pycblib.check_hmac(secret, text, h)
    assert is_true
