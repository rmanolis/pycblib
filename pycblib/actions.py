import os
from ctypes import CDLL, Structure, c_char_p, c_int, c_ubyte, c_byte, \
    create_string_buffer, pointer
from distutils.sysconfig import get_python_inc

lib_file = os.path.join(os.path.dirname(get_python_inc()),
                        'lib', 'libcbdsa.so')
lib = CDLL(lib_file)


P256 = "P256"
P384 = "P384"
P521 = "P521"


class Key(object):
    privateKey = str()
    publicKey = str()


class CKey(Structure):
    _fields_ = [("PrivateKey", c_char_p),
                ("PublicKey", c_char_p)]


def generate_keys(curve: str) -> Key:
    c = CKey()
    pc = pointer(c)
    scurve = create_string_buffer(curve.encode())
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_generate_keys(scurve, pc, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    k = Key()
    k.privateKey = c.PrivateKey.decode()
    k.publicKey = c.PublicKey.decode()
    return k


def sign(privateKey: str, file: bytearray) -> str:
    cpk = create_string_buffer(privateKey.encode())
    buff = (c_byte * len(file)).from_buffer(file)
    clen = c_int(len(file))
    signature = c_char_p()
    ps = pointer(signature)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_sign(cpk, buff, clen, ps, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return signature.value.decode()


def verify(publicKey: str, file: bytearray, signature: str) -> bool:
    cpk = create_string_buffer(publicKey.encode())
    buff = (c_byte * len(file)).from_buffer(file)
    clen = c_int(len(file))
    cs = create_string_buffer(signature.encode())
    verified = c_int(0)
    pv = pointer(verified)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_verify(cpk, buff, clen, cs, pv, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return verified.value > 0


def decrypt(privateKey: str, file: bytearray) -> bytearray:
    cpk = create_string_buffer(privateKey.encode())
    buff = (c_byte * len(file)).from_buffer(file)
    clen = c_int(len(file))
    decoded = c_char_p()
    pd = pointer(decoded)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_decrypt(cpk, buff, clen, pd, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return bytearray(decoded.value)


def encrypt(publicKey: str, file: bytearray) -> bytearray:
    cpk = create_string_buffer(publicKey.encode())
    buff = (c_byte * len(file)).from_buffer(file)
    clen = c_int(len(file))
    encoded = c_char_p()
    pe = pointer(encoded)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_encrypt(cpk, buff, clen, pe, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return bytearray(encoded.value)


def encrypt_cbc(secret: str, text: str) -> str:
    cs = create_string_buffer(secret.encode())
    ct = create_string_buffer(text.encode())
    encoded = c_char_p()
    pe = pointer(encoded)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_encrypt_cbc(cs, ct, pe, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return encoded.value.decode()


def decrypt_cbc(secret: str, text: str) -> str:
    cs = create_string_buffer(secret.encode())
    ct = create_string_buffer(text.encode())
    decoded = c_char_p()
    pe = pointer(decoded)
    err = c_char_p()
    perr = pointer(err)
    has_err = lib.cb_decrypt_cbc(cs, ct, pe, perr)
    if has_err > 0:
        raise Exception(err.value.decode())

    return decoded.value.decode()


def compute_hmac(secret: str, text: str) -> str:
    cs = create_string_buffer(secret.encode())
    ct = create_string_buffer(text.encode())
    hmac = c_char_p()
    ph = pointer(hmac)
    lib.cb_compute_hmac(cs, ct, ph)
    return hmac.value.decode()


def check_hmac(secret: str, text: str, hmac: str) -> bool:
    cs = create_string_buffer(secret.encode())
    ct = create_string_buffer(text.encode())
    ch = create_string_buffer(hmac.encode())
    i = lib.cb_check_hmac(cs, ct, ch)
    return i > 0
